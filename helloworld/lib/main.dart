import 'package:flutter/material.dart';

// 程式進入點
// 透過 runApp() 執行 app widget
void main() => runApp(new MyApp());

// 定義 app widget
// #1 在 app widget 中，需要定義 tile, theme, home
// #2 StatelessWidget 需要提供實作 build 方法，
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      // 定義 title
      title: 'Welcome to Flutter',

      // 定義主題
      theme: new ThemeData(
        primaryColor: Colors.lightBlue,
      ),

      // 定義 homepage widget
      // #1 使用 Scaffold類 提供基本的元素，
      //    不一定要用Scaffold類，但Scaffold類已經提供基本的組件，比較方便
      home: new Scaffold(
        appBar: new AppBar(
          title: new Text('This is the title text'),

        ),
        body: new Center(
          child: new Text('Hello World'),
        ),
      ),
    );
  }
}

