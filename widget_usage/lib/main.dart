/*
  Ref:
  https://ithelp.ithome.com.tw/users/20111840/ironman/1740，使用flutter建構Android和iOs APP 系列
  https://ithelp.ithome.com.tw/users/20096484/ironman/2699，Flutter 程式設計入門實戰 30 天 系列
  https://ithelp.ithome.com.tw/users/20119550/ironman/2221，Flutter---Google推出的跨平台框架，Android、iOS一起搞定
  https://ithelp.ithome.com.tw/users/20121111/ironman/2861，前端工程師的 Flutter 新手村挑戰
  https://ithelp.ithome.com.tw/users/20103043/ironman/2186，用 Flutter 開發一個 Android App 吧
  https://dartpad.dartlang.org/，線上測試 dart 語法
  https://flutterchina.club/，Flutter 中文網


  # 組件，將所有元件組合起來的集合體
    元件，無法再被添加的最小元件

  # 常用widget
    ==== 自定義元件 ====

    ==== layout用 ====
    Row widget:讓元件水平排列的組件，不能捲動，不能自動填滿
    Column widget:讓元件垂直排列的組件，不能捲動，不能自動填滿
    Expanded widget:自動填滿，必須在 Column 或 Row widget 中使用
    ListView widget:可以捲動
    Center widget:置中組件
    Container widget:可自動計算邊界的容器組件

    ==== 圖標圖示 ====
    const FlutterLogo()，內建圖示
    Icon widget:顯示圖標，例，Icon(類別，顏色，大小)，內建圖標
    Image widget:顯示圖片，例，Image(image:來源, width:寬, height:高)

    ==== 按鈕 ====
    RaisedButton(onPressed: 要執行的函數, textColor: 顯示顏色, padding: 設定邊界, child: 建立 Text Widget)

    ==== 特殊用途 ====
    Scaffold，用來快速設計佈局的Ｗidget，提供快速建立以下元件
      appBar：標題列，預設在上方顯示的AppBar或是ToolBar。
        leading，標題圖示
        title，標題文字
        actions，添加標題區要執行的動作
        bottom，標題區底部
        flexibleSpace，標題區中間區域
      body：當前Widget顯示的內容。
      floatingActionButton：預設顯示在右下角的主按鈕。
      bottomNavigationBar：顯示在畫面底部的導航欄。
      bottomSheet：這是一個畫面底部彈出的頁面，透過ScaffoldState.showBottomSheet控制。
      drawer：側邊欄的元件。
      snackBar：一個暫時性的訊息通知，顯示在畫面下方，透過ScaffoldState.showSnackBar控制。
      scaffoldState：跟當前Widget相關的狀態。
*/
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

// App 的根組件
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      // 註冊 home 組件
      home: MyHomePage(title: '這是標題列文字'),
    );
  }
}

// 定義 Home 組件
// 利用狀態的改變來更新每次點擊按鈕的次數
class MyHomePage extends StatefulWidget {
  // Home 組件的靜態顯示部分
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  // Home 組件用來儲存動態資料的部分
  // 在 StatefulWidget 中，必須顯式手動調用 createState()，並返回狀態組件，State<綁定的StatefulWidget名稱>
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

// 定義_MyHomePageState 組件，建立私有的常數 _counter，用來儲存狀態
class _MyHomePageState extends State<MyHomePage> {
  // 定義儲存狀態的私有變數
  int _counter = 0;

  void _incrementCounter() {
    // 狀態組件內建的 setState()，用來更新定義更新狀態的方法，Flutter会调用build()，更新显示並主動更新相關的UI
    setState(() {
      _counter++;
    });
  }

  void _clear() {
    // 狀態組件內建的 setState()，用來更新定義更新狀態的方法，並主動更新相關的UI
    setState(() {
      _counter = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // 頂層的標題列區
      appBar: AppBar(
        leading: Icon(Icons.favorite, color: Colors.pink, size: 34),
        title: Text(widget.title),
      ),

      // 定義中間顯示區的內容
      body: Center(
        // 建立 Column 組件，layout 用
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          // 透過 <Widget>[] 將多個widget 放在一起
          children: <Widget>[
            // 顯示圖片
            Image(
              width: 200,
              height: 200,
              image: NetworkImage(
                  'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl.jpg'),
            ),

            // 顯示歸零按鈕
            RaisedButton(
              onPressed: _clear,
              textColor: Colors.black,
              padding: const EdgeInsets.all(10.0),
              child: const Text('Clear', style: TextStyle(fontSize: 20)),
            ),

            // 顯示文字
            Text(
              '點擊按鈕:',
            ),

            // 顯示數字
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),

      // 顯示浮動按鈕
      floatingActionButton: FloatingActionButton(
        // 按下按鈕時，調用_incrementCounter()
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
