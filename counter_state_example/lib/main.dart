/*
#   Widget類延伸出   ->  StatelessWidget  ：用來 1_建立App 2_建立不需要狀態的Widget
                    ->  StatefullWidget  ：代表該widget類中，會有 createState()，需要建立 state-object

#   state-object 必須透過 State<類型> 的類模板來建立
    State<類型> 類模板也是繼承自 StatefulWidget類
    利用 state-object 來管理 ＭＶＣ 架構中的各個組件

#   利用 runApp()，執行建立的 App
#   利用 MaterialApp類，建立 App 的主要畫面，可以設定
    home, routes, title, theme, locale...

#   組件的繼承關係
    AppBar  ->  StatefulWidget + PreferredSizeWidget
    Center  ->  Align ->  SingleChildRenderObjectWidget ->  RenderObjectWidget  ->  Widget
    Column  ->  Flex  ->  MultiChildRenderObjectWidget  ->  RenderObjectWidget  ->  Widget
    Text    ->  StatelessWidget -> Widget
    FloatingActionButton  ->  StatefulWidget  -> Widget
*/

import 'package:flutter/material.dart';

// main() 程式主要進入點
// 透過 runApp() 來執行建立的 app
// runApp()中會執行 WidgetsFlutterBinding類的方法，並對 app 進行
//    1_初始化
//    2_將app加到RootWidget中
//    3_將 app 加到 Frame 中
void main() => runApp(new MyApp());



// ==== 利用 MaterialApp類，建立 App ====
// App 是不能有狀態的，所以用 StatelessWidget
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    // 利用 MaterialApp 類來建立 app
    return new MaterialApp(
      title: 'Flutter',

      // 設定默認主題
      theme: new ThemeData(
        primarySwatch: Colors.cyan,
      ),

      // 顯示主要頁面
      home: new MyHomePage(title: 'Hello ching'),
    );
  }
}



// ===== 透過 StatefulWidget建立主要顯示頁面 =====
// Stateful 代表該類會有 state-object 來管理狀態
// StatefulWidget 才會有 createState() 的方法，用來建立 state-object
class MyHomePage extends StatefulWidget {

  // MyHomePage類的構造方法
  MyHomePage({Key key, this.title}) : super(key: key);

  // 宣告變數
  final String title;

  // 覆寫 StatefulWidget類中的 createState()
  // 在StatefulWidget類中，使用crateState()方法來建立 state-object
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}



// ===== 建立 MyHomePage 的 state-object ======
// state-object 必須透過 State<類型> 的類模板來建立
// State<類型> 類模板也是繼承自 StatefulWidget類
// 利用 state-object 來管理 ＭＶＣ 架構中的各個組件
class _MyHomePageState extends State<MyHomePage> {

  // Model : 用來儲存 counter 狀態的變數
  int _counter = 0;

  // Controller 用來變更狀態
  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  // Viewe
  @override
  // 每次 setState() 被呼叫時，會自動調用 build 函數
  Widget build(BuildContext context) {

    // ===== scaffold 類：與layout相關的框架 =====
    // scaffold類繼承自StatefulWidget類
    return new Scaffold(

      // 元件1 : 建立 appBar
      // AppBar類，繼承自 StatefulWidget類，並實作 PreferredSizeWidget類
      appBar: new AppBar(
        title: new Text(widget.title),
      ),

      // 元件2 : 建立 Center layout
      body: new Center(

        // 元件3 : 建立 Column layout
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,

          // 元件4 : 建立 Text
          // 透過 <Widget>[ ] 建立多個 widget
          children: <Widget>[
            new Text(
              'Click on button below will crease number:',
            ),
            new Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),
          ],


        ),
      ),

      // 元件5 : 添加浮動按鈕
      floatingActionButton: new FloatingActionButton(
        // 按下按鈕後，執行 _incrementCounter()
        onPressed: _incrementCounter,

        // For debug
        tooltip: 'Increment',

        // 浮動案糗樣式
        child: new Icon(Icons.add_box),
      ),

    );
  }
}
