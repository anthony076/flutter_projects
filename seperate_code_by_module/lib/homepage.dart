
import 'package:flutter/material.dart';

// define MyHomePage widget
class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

// define state-object for MyHomePage widget
class _MyHomePageState extends State<MyHomePage> {
  // define Model
  int _counter = 0;

  // define Controller
  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  // define View
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      // defince appBar
      appBar: new AppBar(title: new Text(widget.title)),
      // defince body
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>
          [
            new Text('Click on button below will crease number:'),
            new Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),

      //define floatingActionButton
      floatingActionButton: new FloatingActionButton(
        onPressed: _incrementCounter,
        // For debug
        tooltip: 'Increment',
        // 浮動按鈕的樣式
        child: new Icon(Icons.add_box),
      ),

    );
  }
}