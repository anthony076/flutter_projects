import 'package:flutter/material.dart';
import 'homepage.dart';

void main() => runApp(new MyApp());

// ==== 利用 MaterialApp類，建立 App ====
// App 是不能有狀態的，所以用 StatelessWidget
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    // 利用 MaterialApp 類來建立 app
    return new MaterialApp(
      title: 'Flutter',

      // 設定默認主題
      theme: new ThemeData(
        primarySwatch: Colors.cyan,
      ),

      // 顯示主要頁面
      home: new MyHomePage(title: 'Hello ching'),
    );
  }
}



